# Photometry of Optical Variables with Gaia Data Release 2

This project, which began on May 28th 2019, is dedicated to finding various trends in Gaia Data Release 2 that will help facillitate searches for optical variables. This project was motivated for the search for redback systems, which are typically characterized by the presence of a companion optical variable star, but will likely have many other practical applications in the study of variable sources.  
  
The specific goals of this project are as follows:

* Determining how variable stars map onto a magnitude vs magnitude error plot (as measured by Gaia DR2) as a function of various parameters, including amplitude and optical type. This is currently contained in the **Variable Stars Branch** and uses data from WISE and Catalina catalogs.

* Determining how the "baseline curve" shifts in position *and* changes in slope as a function of various parameters, including: the number of observations contributing to G photometry for each source, the density, and stellar position ("Baseline curve" refers to the data points of non-optical stars in a magnitude vs magnitude error plot). This is currently contained in the **Baseline Curve Branch**. 

* Determining what parameters, other than the variability of a star, can cause a source to be "off" the baseline curve and have a higher magnitude error. This is currently contained in the **False Positives Branch**. 